const apolloClient = require( "../config/apolloClient");
const gql = require("graphql-tag");
const jwt = require("jsonwebtoken");
const secretkey = 'thisisniscalbabubhorastudentofsamriddhi';

module.exports = {
    fetchUser:async (email) => {
        const query = gql(`
                query MyQuery {
                  users(where: {email: {_eq: "${email}"}}) {
                    email
                    id
                  }
                }
        `)
        try {
            const result = await apolloClient.query({
                query,
            });
            return result.data.users
        } catch (err) {
            return err
        }
    },
    generateToken: (data) => {
        const payload = {
            "name":data.email,
            "id":data.id,
            "https://hasura.io/jwt/claims": {
                "x-hasura-allowed-roles": ["student", "teacher"],
                "x-hasura-default-role": "student",
                "x-hasura-user-id": `${data.id}`,
            }
        }
        return jwt.sign(
            payload,
            secretkey,
            {
                expiresIn: 3600 * 24
            },
        )
    }
}
