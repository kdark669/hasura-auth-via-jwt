const ApolloClient = require("apollo-client").ApolloClient;
const createHttpLink = require("apollo-link-http").createHttpLink;
const fetch = require("node-fetch");
const InMemoryCache = require("apollo-cache-inmemory").InMemoryCache;

const db_uri = "https://together-crawdad-14.hasura.app/v1/graphql"
const httpLink = createHttpLink({
    uri: db_uri,
    fetch: fetch,
    headers: {
        'Content-Type': 'application/json',
        'Hasura-Client-Name': "hasura-console",
        'x-hasura-admin-secret': "@tinuS123",
    }
});

const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache()
});

module.exports = client;
