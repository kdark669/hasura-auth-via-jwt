const {
    fetchUser,
    generateToken
} = require('../Helpers/AuthHelpers')

module.exports = {
    login: async (req, res, next) => {
        try{
            const { email } = req.body
            const user = await fetchUser(email)
            if (user.length === 0) {
                console.log('error no user')
            }
            const token = await generateToken(user[0])
            return res.status(200).json({
                status: "success",
                token: token,
                user: user[0]
            })
        }catch (e) {
            console.log(e)
        }
    }
}


