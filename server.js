const express = require('express')
const dotenv = require('dotenv')
dotenv.config()
const cors = require('cors')
const passport = require('passport');
const app = express()
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

app.use(cors())
app.use(passport.initialize());
require('./routes')(app)

module.exports = app;
